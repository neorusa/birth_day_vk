# -*- coding: utf-8 -*-
# -------------------------------------------------------------------------
# This is a sample controller
# this file is released under public domain and you can use without limitations
# -------------------------------------------------------------------------
from vk_post import VkPost


@auth.requires_login()
def index():
    response.flash = T("Hello World")
    return dict(message=T('Welcome to web2py!'))


@auth.requires_login()
def edit_posts():
    response.title = 'Новые посты'
    grid = SQLFORM.grid(db.posts, csv=False, searchable=False)
    return locals()


# @auth.requires_login()
# def test():
#     post = db(db.posts.id > 0).select(orderby='<random>').first()
#     return post.id


@auth.requires_login()
def tester():
    vk = VkPost()
    res, count, bday, bmounth = vk.get_friends()
    return dict(res=res, bday=bday, bmounth=bmounth, count=count)


# ---- Action for login/register/etc (required for auth) -----
def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/bulk_register
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    also notice there is http://..../[app]/appadmin/manage/auth to allow administrator to manage users
    """
    return dict(form=auth())

# ---- action to server uploaded static content (required) ---
@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)
