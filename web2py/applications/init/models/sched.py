# -*- coding: utf-8 -*-
# Author: Ruslan Gareev (neorusa@ya.ru)
from vk_post import VkPost
from random import randint
from datetime import timedelta as td
import json
# -------------------------------------------------------------------------
# use the scheduler
# -------------------------------------------------------------------------
from gluon.scheduler import Scheduler
scheduler = Scheduler(db, heartbeat=configuration.take('scheduler.heartbeat', cast=int))


# Постим юзеру на стену
def post(user, post_id):
    vk = VkPost()
    vk.send_post(user, post_id)


# Отправляем сообщением
def send_pm(user, post_id):
    vk = VkPost()
    vk.send_pm(user, post_id)


# Получим юзеров у кого сегодня др
def get_users():
    vk = VkPost()
    users, count = vk.get_friends()

    if count > 0:
        delta = 120  # ставим разницу в 2 минуты, ибо вк - капча
        now = request.now
        for user in users:
            curr_post = db(db.posts.id > 0).select(orderby='<random>').first()
            if int(user['can_write_private_message']) == 1:
                db.scheduler_task.validate_and_insert(
                    function_name='send_pm',
                    start_time=now + td(seconds=delta),
                    args=json.dumps([user, curr_post.id])
                )
                db.commit()
            delta += 120
