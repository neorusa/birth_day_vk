# -*- coding: utf-8 -*-
# Author: Ruslan Gareev (neorusa@ya.ru)
import vk_api
from gluon import current
import time
import os
ad = """
И еще. Я работаю в чип-тюнинг ателье "Chip5.PRO".
И в связи с днем рождения и только сегодня - 15% скидка на прошивку ЭБУ вашего автомобиля.
Результат от прошивки:
- Увеличивается мощность до +30%.
- Снижается расход при спокойной езде (от 0.5 до 2 литров)
- Возможно программно отключить EGR, сажевый фильтр или катализатор, замена которых стоит дорого;
- Улучшается общая динамика автомобиля;
- Улучшается динамика при включенном кондиционере;
- Сглаживаются переключения коробки передач;
- Повышается чувствительность педали газа;
- Исчезает турбояма на турбированных двигателях;
- На низких оборотах вырастает тяга;
- Исчезают провалы при разгоне.
- Без ущерба для двигателя и других агрегатов!

Подробнее в моей группе: https://vk.com/chip5pro

Если вступите в группу и сделаете репост закрепленной записи, то еще +5% скидка)
"""


class VkPost(object):
    def __init__(self):
        self.vk_session = vk_api.VkApi(
            token=current.config.get('vk.access_token'),
            app_id=current.config.take('vk.appid', cast=int)
        )
        self.error = None
        self.vk = None

        self.vk = self.vk_session.get_api()

    # Получим список друзей, у кого сегодня др
    def get_friends(self):
        if self.error:
            return self.error

        vk = self.vk

        bday = current.request.now.strftime('%d')
        bmounth = current.request.now.strftime('%m')

        response = vk.users.search(
            count=100,
            fields='can_post,can_write_private_message',
            birth_day=bday,
            birth_month=bmounth,
            from_list='friends'
        )

        if response['items']:
            return response['items'], int(response['count'])
        else:
            return None, 0

    # Отправим пост на стену другу
    def send_post(self, friend, post_id):
        if self.error:
            return self.error

        vk = self.vk
        db = current.db
        request = current.request

        # Получим пост из БД
        post = db(db.posts.id == post_id).select().first()
        message = post.description
        image = post.image

        # Загрузим картинку на сервера вк
        upload = vk_api.VkUpload(self.vk_session)
        photo = upload.photo_wall(
            photos=os.path.join(request.folder, 'uploads', image),
            user_id=friend['id'],
        )
        photo = photo[0]

        # Запостим на стену юзеру
        response = vk.wall.post(
            owner_id=friend['id'],
            message=message,
            attachments='photo' + str(photo['owner_id']) + '_' + str(photo['id'])
        )

        if response['post_id']:
            return True
        elif response['error']:
            return response['error']['error_msg']

    # Отправим сообщением поздравление
    def send_pm(self, friend, post_id):
        if self.error:
            return self.error

        vk = self.vk
        db = current.db
        request = current.request

        # Получим пост из БД
        post = db(db.posts.id == post_id).select().first()
        message = post.description
        image = post.image

        # Загрузим картинку на сервера вк
        upload = vk_api.VkUpload(self.vk_session)
        photo = upload.photo_messages(
            photos=os.path.join(request.folder, 'uploads', image),
        )
        photo = photo[0]

        # Запостим на стену юзеру
        response = vk.messages.send(
            user_id=friend['id'],
            message=message,
            attachment='photo' + str(photo['owner_id']) + '_' + str(photo['id'])
        )

        if response:
            return True
        else:
            return 'Error'

